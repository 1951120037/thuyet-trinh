<!DOCTYPE html>

<html lang="en">
    <header>
        <meta charset="UFT-8">
        <meta mame="viewport" content="width=divice-width, initial-scale=1.0">
        <title> RealTime Chat App | Login</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
    </header>
    <body>
        <div class="wrapper">
            <section class="form login">
                <header>Realtime Chat App</header>
                <form action="a">
                    <div class="error-txt">This is an error message!</div>
                    <div class="field input">
                        <lable>Email Address</lable>
                        <input type="text" placeholder="Enter your email">
                    </div>
                    <div class="field input">
                        <lable>Password</lable>
                        <input type="password" placeholder="Enter your password">
                        <i class="fas fa-eye"></i>
                    </div>
                    <div class="field button">
                        <input type="submit" value="Continue to Chat">
                    </div>
                </form>
                <div class="link">Not yet signed up?<a href="#"> Signup now</a></div>

            </section>
        </div>

        <script src="javascript/pass-show-hide.js"></script>
    </body>
</html>