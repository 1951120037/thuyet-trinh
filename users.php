<!DOCTYPE html>

<html lang="en">
    <header>
        <meta charset="UFT-8">
        <meta mame="viewport" content="width=divice-width, initial-scale=1.0">
        <title> RealTime Chat App | About</title>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" />
    </header>
    <body>
        <div class="wrapper">
            <section class="users">
                <header>
                    <div class="content">
                        <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                        <div class="details">
                            <span >LongCoding</span>
                            <p >Active now</p>
                        </div>
                    </div>
                    <a href="#" class="logout">Logout</a>
                </header>
                <div class="search">
                    <span class="text">Select an user to start chat</span>
                    <input type="text" placeholder="Enter name to search...">
                    <button><i class="fas fa-search"></i></button>
                </div>
                <div class=" users-list">
                    <a href="#">
                        <div class="content">
                            <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                            <div class="details">
                                <span>LongCoding</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fas fa-circle"></i></div>
                    </a>
                    <a href="#">
                        <div class="content">
                            <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                            <div class="details">
                                <span>LongCoding</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fas fa-circle"></i></div>
                    </a>
                    <a href="#">
                        <div class="content">
                            <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                            <div class="details">
                                <span>LongCoding</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fas fa-circle"></i></div>
                    </a>
                    <a href="#">
                        <div class="content">
                            <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                            <div class="details">
                                <span>LongCoding</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fas fa-circle"></i></div>
                    </a>
                    <a href="#">
                        <div class="content">
                            <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                            <div class="details">
                                <span>LongCoding</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fas fa-circle"></i></div>
                    </a>
                    <a href="#">
                        <div class="content">
                            <img src="img/KQe1RaJ_XDmgl5UP__2500x1827.jpg" alt="">
                            <div class="details">
                                <span>LongCoding</span>
                                <p>This is test message</p>
                            </div>
                        </div>
                        <div class="status-dot"><i class="fas fa-circle"></i></div>
                    </a>
                </div>
            </section>
        </div>

        <script src="javascript/users.js"></script>
    </body>
</html>